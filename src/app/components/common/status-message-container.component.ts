import {Component, Input, Output, Inject} from '@angular/core';

@Component({
    selector: 'status-message-container',
    templateUrl: '../../templates/common/status-message-container.template.html'
})

export /**
 * StatusMessageContainerComponent
 */
    class StatusMessageContainerComponent {
    @Input() condition: any;
    @Input() type: String
    @Input() title: String
    @Input() messages: Array<any>

    constructor() {
    }
}