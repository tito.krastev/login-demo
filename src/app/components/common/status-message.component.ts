import {Component, Input, Output, Inject} from '@angular/core';

@Component({
    selector: 'status-message',
    templateUrl: '../../templates/common/status-message.template.html'
})

export /**
 * StatusMessageComponent
 */
    class StatusMessageComponent {
    @Input() condition: Boolean

    constructor() {
    }
}