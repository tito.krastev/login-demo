import {NgModule, CUSTOM_ELEMENTS_SCHEMA}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule}   from '@angular/forms';
import {HttpModule} from '@angular/http';
import {StatusMessageComponent} from './status-message.component';
import {StatusMessageContainerComponent} from "./status-message-container.component";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule
    ],
    declarations: [
        StatusMessageComponent,
        StatusMessageContainerComponent
    ],
    exports: [StatusMessageComponent,
        StatusMessageContainerComponent]
})
    
export class CommonModule { }