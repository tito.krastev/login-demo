import { Component, OnInit } from '@angular/core';
import {Injectable, Inject, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService } from '../../services/login.service';
import {ValidatorService} from '../../services/validator.service';
import {ShowHideInputDirective} from '../../directives/show-hide-input.directive'
import {NgForm, FormBuilder, FormControl, FormGroup, Validators}  from '@angular/forms';

@Component({
    selector: 'login',
    templateUrl: '../../templates/login/login.component.html',
    styleUrls: ['../../../assets/css/login.component.css']
})
export class LoginComponent implements OnInit {
    form: FormGroup
    email: FormControl
    password: FormControl
    submitted: boolean
    showPassword: boolean
    failedLogin: boolean
    @ViewChild(ShowHideInputDirective) input: ShowHideInputDirective;

    constructor(@Inject(LoginService) private loginService: LoginService, 
        private builder: FormBuilder, private router: Router,) {
        this.submitted = false;
        this.failedLogin = false;
        this.showPassword = false;
        this.email = new FormControl(null, Validators.compose([Validators.required, Validators.maxLength(50), ValidatorService.invalidEmailFormat]), null);
        this.password = new FormControl(null, Validators.compose([Validators.required,Validators.minLength(6), Validators.maxLength(18), ValidatorService.invalidPasswordFormat]), null);
        this.form = builder.group({
            email: this.email,
            password: this.password
        });
    }

    ngOnInit() {
    }


    login() {
        this.submitted = true;
        var username = this.email.value;
        var pass = this.password.value;

        if (this.form.valid) {
            console.log(username);
            console.log(pass);
        }
    }

    toggleShowPassword()
    {
        this.showPassword = !this.showPassword;
        console.log(this.input); //undefined
        if (this.showPassword){
            this.input.changeType("text");
        }
        else {
            this.input.changeType("password");
        }
    }
}
