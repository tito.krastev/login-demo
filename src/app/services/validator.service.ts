import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';



interface ValidationResult {
    [key: string]: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
    private static emailRegex = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.([a-z]{1,20})|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
    private static passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])/;
    private static nonAlphaSymbols = /[^A-Za-z\s]+/;
    private static onlyNumeric = /[^0-9]+/;
    private static nonAlphaNumericSymbols = /[^A-Za-z0-9\s]+/;
    private static atLeastOneSmallLetter = /^(?=.*[a-z]).+$/;
    private static atLeastOneCapitalLetter = /^(?=.*[A-Z]).+$/;
    private static atLeastOneSpecialCharacter = /^(?=.*(_|[^\w])).+$/;
    private static atLeastOneNumber = /^(?=.*\d).+$/;
    private static validNamePattern = /^[A-Z].[A-Za-z\-\s\'.]+$/;
    private static validDonorNamePattern = /^[A-Za-z\u0080-\u00FF]+[A-Za-z\-\s\'.\s\&\u0080-\u00FF]*$/;
    private static validOnboardingNamePattern = /^[a-zA-Z0-9]+$/;
    private static validOnboardingPasswordPattern = /^[a-zA-Z0-9]+$/;
    private static phoneNumberPattern = /^\+?\d{3,20}$/;
    private static validCurrencyAmountPattern = /^[0-9]+(\.[0-9]{1,2})?$/;
    private static validUrlRegex = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;

    constructor(parameters:any) {
        
    }

    public static invalidEmailFormatNotCheckNull(control: FormControl): ValidationResult {
        if(control.value !== null && control.value.trim() !== "" && !ValidatorService.emailRegex.exec(control.value)){
            return { "invalidEmailFormatNotCheckNull": true };
        }
        
        return null;
    }

    public static invalidEmailFormat(control: FormControl): ValidationResult {
        if(!ValidatorService.emailRegex.exec(control.value)){
            return { "invalidEmailFormat": true };
        }
        
        return null;
    }
    
    public static invalidPasswordFormat(control: FormControl): ValidationResult {
        if(!ValidatorService.passwordRegex.exec(control.value)){
            return { "invalidPasswordFormat": true };
        }
        
        return null;
    }

    public static invalidSmallLetterFormat(control: FormControl): ValidationResult {
        if (!ValidatorService.atLeastOneSmallLetter.exec(control.value)) {
            return { "invalidSmallLetterFormat": true };
        }

        return null;
    }

    public static invalidCapitalLetterFormat(control: FormControl): ValidationResult {
        if (!ValidatorService.atLeastOneCapitalLetter.exec(control.value)) {
            return { "invalidCapitalLetterFormat": true };
        }

        return null;
    }

    public static invalidSpecialCharacterlFormat(control: FormControl): ValidationResult {
        if (!ValidatorService.atLeastOneSpecialCharacter.exec(control.value)) {
            return { "invalidSpecialCharacterlFormat": true };
        }

        return null;
    }

    public static invalidNumberFormat(control: FormControl): ValidationResult {
        if (!ValidatorService.atLeastOneNumber.exec(control.value)) {
            return { "invalidNumberFormat": true };
        }

        return null;
    }
    
    public static required(control: FormControl): ValidationResult {
        if(!control.value || !control.value.trim()){
            return { "required": true };
        }
    }
}